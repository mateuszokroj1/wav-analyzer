﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using static System.Math;

namespace WaveFormat
{
    /// <summary>
    /// Fast Fourier Transformation for sound
    /// </summary>
    public class FrequencyAnalyzer
    {
        public FrequencyAnalyzer(IEnumerable<float> input)
        {
            if (input == null)
                throw new ArgumentNullException();
            if (input.Count() < 4)
                throw new ArgumentException("Array length is to small for FFT transformation.");

            this.samples = input.ToArray();
        }

        private float[] samples;

        public double GetFrequencyPoint(float frequency)
        {
            if (frequency < 1f) throw new ArgumentException("Argument is to small");
            if (frequency > 100000f) throw new ArgumentException("Argument is to large");
            Complex sum = 0;
            uint count = (uint)samples.Count();
            Parallel.For(0, (int)count, new Action<int>(i=>
            {
                sum += samples[i] * Complex.Pow(Math.E, -(new Complex(0,2 * Math.PI)/samples.Count()) * i * frequency);
            }));
            return sum.Magnitude;
        }
        public IEnumerable<double> GetFrequencyPoints(float[] points) => points.AsParallel().AsOrdered().Select(point=> GetFrequencyPoint(point)).AsEnumerable();
    }
}
