﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaveFormat
{
    [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand)]
    public abstract class WaveFormatBase : IDisposable
    {
        #region Constructors
        public WaveFormatBase(FileStream file)
        {
            if (file == null)
                throw new ArgumentNullException("file");
            if (!file.CanRead)
                throw new IOException("Cannot read this file.");
            this.file = file;
            this.reader = new BinaryReader(file);
        }
        #endregion

        #region Destructor
        ~WaveFormatBase() { this.Dispose(); }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                reader.Dispose();
                file.Dispose();

                disposed = true;
            }
        }
        public void Dispose() => this.Dispose(true);
        #endregion

        #region Fields
        protected bool disposed = false;
        protected FileStream file;
        protected BinaryReader reader;
        protected ulong samplescount = 0;
        protected byte bitdepth = 0;
        protected byte channels = 0;
        protected uint samplerate = 0;
        protected uint byterate = 0;
        protected ushort blockalign = 0;
        protected ulong dataStartpos = 0;
        #endregion

        #region Properties
        /// <summary>
        /// File Stream
        /// </summary>
        public FileStream File => file;

        /// <summary>
        /// Bit depth of one sample
        /// </summary>
        public byte BitDepth => bitdepth;

        public ulong SamplesCount => samplescount;

        public uint SampleRate => samplerate;

        public byte Channels => channels;
        #endregion
    }

    public interface IWaveFormat<T>
    {
        #region Methods
        T GetSample(byte channelIndex, ulong index);

        /// <summary>
        /// Returns all samples as two-dimensional array [index,channel]
        /// </summary>
        T[,] GetSamplesAsArray();

        /// <summary>
        /// Returns task with all samples as two-dimensional array [index,channel]
        /// </summary>
        Task<T[,]> GetSamplesAsArrayAsync();

        Task<T[,]> GetSamplesAsArrayAsync(CancellationToken cancel, IProgress<float> progress);
        #endregion
    }

    /// <summary>
    /// Reads RIFF WAVE PCM file with 8/16/24/32 bit samples.
    /// </summary>
    public class WavePCMFormat : WaveFormatBase, IWaveFormat<int>
    {
        private readonly object _locker = new object();
        #region Constructor
        public WavePCMFormat(FileStream File) : base(File)
        {
            /* Reading settings from file */
            file.Position = 0;
            byte[] bytes = reader.ReadBytes(4);
            if (Encoding.ASCII.GetString(bytes) != "RIFF") // "RIFF"
                throw new WaveFormatException("Plik nie jest formatu RIFF WAVE PCM.");

            file.Position += 4;
            bytes = reader.ReadBytes(4);
            if (Encoding.ASCII.GetString(bytes) != "WAVE") // "WAVE"
                throw new WaveFormatException("Plik nie jest formatu RIFF WAVE PCM.");

            /* Subchank1 */
            bytes = reader.ReadBytes(4);
            if (Encoding.ASCII.GetString(bytes) != "fmt ") // "fmt "
                throw new WaveFormatException("Plik nie jest formatu RIFF WAVE PCM.");
            int fmt_size = reader.ReadInt32() - 16;

            if (reader.ReadInt16() != 1) throw new WaveFormatException("Plik RIFF WAVE nie jest w formacie PCM.");

            channels = Convert.ToByte(Math.Min(reader.ReadInt16(), byte.MaxValue));

            samplerate = (uint)Math.Abs(reader.ReadInt32());

            byterate = (uint)Math.Max(reader.ReadInt32(), 0);

            blockalign = (ushort)Math.Max((int)reader.ReadInt16(), 0);

            bitdepth = (byte)Math.Abs(Math.Min(reader.ReadInt16(), byte.MaxValue));

            /* DATA Subchank */
            if (fmt_size > 0)
                file.Position += fmt_size;

            bytes = reader.ReadBytes(4);
            if (Encoding.ASCII.GetString(bytes) != "data") // "data"
                throw new WaveFormatException("Bad WAVE format. No data chunk.");

            int size = reader.ReadInt32();

            samplescount = (ulong)Math.Max(blockalign > 0 ? (size / blockalign) : 0, 0);

            dataStartpos = (ulong)Math.Max(file.Position, 0);
        }
        #endregion

        #region Methods
        public int GetSample(byte channelIndex, ulong index)
        {
            if (channelIndex >= channels) throw new IndexOutOfRangeException("Channel index is to large.");
            lock (this._locker)
            {
                switch (bitdepth)
                {
                    case 8:
                        try
                        { file.Position = Convert.ToInt64(dataStartpos + index * channels) + Convert.ToInt64(channelIndex); }
                        catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                        return (int)(int.MaxValue * ((double)(reader.ReadByte() - 127) / sbyte.MaxValue));
                    case 16:
                        try
                        { file.Position = Convert.ToInt64(dataStartpos + index * 2 * channels) + Convert.ToInt64(channelIndex * 2); }
                        catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                        return (int)(int.MaxValue * ((double)reader.ReadInt16() / short.MaxValue));
                    case 24:
                        try
                        { file.Position = Convert.ToInt64(dataStartpos + index * 3 * channels) + Convert.ToInt64(channelIndex * 3); }
                        catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                        byte[] bytes = reader.ReadBytes(3);
                        int ret = 0;
                        // Sprawdzenie czy występuje jakakolwiek jedynka w kodzie U2
                        BitArray bits = new BitArray(bytes);
                        Func<bool> isanytrue = () =>
                        {
                            for (int i = 1; i < bits.Length; i++)
                                if (bits[i]) return true;
                            return false;
                        };
                        if (bits[0]) // Sprawdzenie czy wartość jest ujemna
                        {
                            if (isanytrue())
                            {
                                ret = 0xFF << 24; // 1111 1111 0000 0000 0000 0000 0000 0000
                                ret |= (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
                                return (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));// 24bit MaxValue in U2 code
                            }
                            else
                            {
                                ret = 0x1 << 31;
                                // Usuwanie jedynki, która była bitem znaku (liczba nie jest zanegowana)
                                ret |= ((bytes[0] & 0b0111_1111) << 16) | (bytes[1] << 8) | bytes[2];
                                return (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));
                            }
                        }
                        else
                        {
                            ret = (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
                            return (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));
                        }
                    case 32:
                        try
                        {
                            file.Position = Convert.ToInt64(dataStartpos + index * 4 * channels) + Convert.ToInt64(channelIndex * 4);
                        }
                        catch (ArgumentOutOfRangeException) { reader.Dispose(); file.Dispose(); throw new IndexOutOfRangeException("Index value is to large"); }
                        return reader.ReadInt32();
                    default:
                        throw new WaveFormatException("Max available bit depth is 32.");
                }
            }
        }

        public int[,] GetSamplesAsArray()
        {
            int[,] arr = new int[samplescount,channels];
            try
            {
                for (ulong i = 0; i < samplescount; i++)
                {
                    for(byte j = 0; j < channels; j++)
                    switch (bitdepth)
                    {
                        case 8:
                            if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * channels); }
                            catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                            arr[i,j] = (int)(int.MaxValue * ((double)(reader.ReadByte() - 127) / sbyte.MaxValue));
                            break;
                        case 16:
                            if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * 2 * channels); }
                            catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                            arr[i,j] = (int)(int.MaxValue * ((double)reader.ReadInt16() / short.MaxValue));
                            break;
                        case 24:
                            if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * 3 * channels); }
                            catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                            byte[] bytes = reader.ReadBytes(3);
                            int ret = 0;
                            // Sprawdzenie czy występuje jakakolwiek jedynka w kodzie U2
                            BitArray bits = new BitArray(bytes);
                            Func<bool> isanytrue = () =>
                            {
                                for (int b = 1; b < bits.Length; i++)
                                    if (bits[b]) return true;
                                return false;
                            };
                            if (bits[0]) // Sprawdzenie czy wartość jest ujemna
                            {
                                if (isanytrue())
                                {
                                    ret = 0xFF << 24; // 1111 1111 0000 0000 0000 0000 0000 0000
                                    ret |= (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
                                    arr[i,j] = (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));// 24bit MaxValue in U2 code
                                }
                                else
                                {
                                    ret = 0x1 << 31;
                                    // Usuwanie jedynki, która była bitem znaku (liczba nie jest zanegowana)
                                    ret |= ((bytes[0] & 0b0111_1111) << 16) | (bytes[1] << 8) | bytes[2];
                                    arr[i,j] = (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));
                                }
                            }
                            else
                            {
                                ret = (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
                                arr[i,j] = (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));
                            }
                            break;
                        case 32:
                            if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * 4 * channels); }
                            catch (ArgumentOutOfRangeException) { reader.Dispose(); file.Dispose(); throw new IndexOutOfRangeException("Index value is to large"); }
                            arr[i,j] = reader.ReadInt32();
                            break;
                        default:
                            throw new WaveFormatException("Max available bit depth is 32.");
                    }
                }
            }
            catch (IndexOutOfRangeException) { return arr; }
            return arr;
        }

        public async Task<int[,]> GetSamplesAsArrayAsync()
        {
            int[,] arr = new int[samplescount, channels];
            try
            {
                for (ulong i = 0; i < samplescount; i++)
                {
                    for (byte j = 0; j < channels; j++)
                        switch (bitdepth)
                        {
                            case 8:
                                if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * channels); }
                                    catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                                arr[i, j] = (int)(int.MaxValue * ((double)(reader.ReadByte() - 127) / sbyte.MaxValue));
                                break;
                            case 16:
                                if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * 2 * channels); }
                                    catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                                arr[i, j] = (int)(int.MaxValue * ((double)reader.ReadInt16() / short.MaxValue));
                                break;
                            case 24:
                                if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * 3 * channels); }
                                    catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                                byte[] bytes = reader.ReadBytes(3);
                                int ret = 0;
                                // Sprawdzenie czy występuje jakakolwiek jedynka w kodzie U2
                                BitArray bits = new BitArray(bytes);
                                Func<bool> isanytrue = () =>
                                {
                                    for (int b = 1; b < bits.Length; i++)
                                        if (bits[b]) return true;
                                    return false;
                                };
                                if (bits[0]) // Sprawdzenie czy wartość jest ujemna
                                {
                                    if (isanytrue())
                                    {
                                        ret = 0xFF << 24; // 1111 1111 0000 0000 0000 0000 0000 0000
                                        ret |= (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
                                        arr[i, j] = (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));// 24bit MaxValue in U2 code
                                    }
                                    else
                                    {
                                        ret = 0x1 << 31;
                                        // Usuwanie jedynki, która była bitem znaku (liczba nie jest zanegowana)
                                        ret |= ((bytes[0] & 0b0111_1111) << 16) | (bytes[1] << 8) | bytes[2];
                                        arr[i, j] = (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));
                                    }
                                }
                                else
                                {
                                    ret = (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
                                    arr[i, j] = (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));
                                }
                                break;
                            case 32:
                                if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * 4 * channels); }
                                    catch (ArgumentOutOfRangeException) { reader.Dispose(); file.Dispose(); throw new IndexOutOfRangeException("Index value is to large"); }
                                arr[i, j] = reader.ReadInt32();
                                break;
                            default:
                                throw new WaveFormatException("Max available bit depth is 32.");
                        }
                }
            }
            catch (IndexOutOfRangeException) { return arr; }
            return arr;
        }

        public async Task<int[,]> GetSamplesAsArrayAsync(CancellationToken cancel, IProgress<float> progress)
        {
            int[,] arr = new int[samplescount, channels];
            try
            {
                for (ulong i = 0; i < samplescount; i++)
                {
                    if (cancel.IsCancellationRequested) return arr;
                    for (byte j = 0; j < channels; j++)
                    {
                        switch (bitdepth)
                        {
                            case 8:
                                if(j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * channels); }
                                catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                                arr[i, j] = (int)(int.MaxValue * ((double)(reader.ReadByte() - 127) / sbyte.MaxValue));
                                break;
                            case 16:
                                if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * 2 * channels); }
                                catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                                arr[i, j] = (int)(int.MaxValue * ((double)reader.ReadInt16() / short.MaxValue));
                                break;
                            case 24:
                                if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * 3 * channels); }
                                catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                                byte[] bytes = reader.ReadBytes(3);
                                int ret = 0;
                                // Sprawdzenie czy występuje jakakolwiek jedynka w kodzie U2
                                BitArray bits = new BitArray(bytes);
                                Func<bool> isanytrue = () =>
                                {
                                    for (int b = 1; b < bits.Length; i++)
                                        if (bits[b]) return true;
                                    return false;
                                };
                                if (bits[0]) // Sprawdzenie czy wartość jest ujemna
                                {
                                    if (isanytrue())
                                    {
                                        ret = 0xFF << 24; // 1111 1111 0000 0000 0000 0000 0000 0000
                                        ret |= (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
                                        arr[i, j] = (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));// 24bit MaxValue in U2 code
                                    }
                                    else
                                    {
                                        ret = 0x1 << 31;
                                        // Usuwanie jedynki, która była bitem znaku (liczba nie jest zanegowana)
                                        ret |= ((bytes[0] & 0b0111_1111) << 16) | (bytes[1] << 8) | bytes[2];
                                        arr[i, j] = (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));
                                    }
                                }
                                else
                                {
                                    ret = (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
                                    arr[i, j] = (int)(int.MaxValue * ((double)ret / 0b0111_1111_1111_1111_1111_1111));
                                }
                                break;
                            case 32:
                                if (j == 0) try { file.Position = Convert.ToInt64(dataStartpos + i * 4 * channels); }
                                catch (ArgumentOutOfRangeException) { reader.Dispose(); file.Dispose(); throw new IndexOutOfRangeException("Index value is to large"); }
                                arr[i, j] = reader.ReadInt32();
                                break;
                            default:
                                throw new WaveFormatException("Max available bit depth is 32.");
                        }
                        progress.Report((i+1)/samplescount);
                    }
                }
            }
            catch (IndexOutOfRangeException) { return arr; }
            return arr;
        }
        #endregion

    }

    /// <summary>
    /// Reads RIFF WAVE with IEEE Float 32bit samples.
    /// </summary>
    public class WaveFloatFormat : WaveFormatBase, IWaveFormat<float>
    {
        private readonly object _locker = new object();
        #region Constructor
        public WaveFloatFormat(FileStream File) : base(File)
        {
            /* Reading settings from file */
            file.Position = 0;
            byte[] bytes = reader.ReadBytes(4);
            if (Encoding.ASCII.GetString(bytes) != "RIFF") // "RIFF"
                throw new WaveFormatException("Plik nie jest formatu RIFF WAVE PCM.");

            file.Position += 4;
            bytes = reader.ReadBytes(4);
            if (Encoding.ASCII.GetString(bytes) != "WAVE") // "WAVE"
                throw new WaveFormatException("Plik nie jest formatu RIFF WAVE PCM.");

            /* Subchank1 */
            long currpos;
            for (currpos = file.Position; currpos < file.Length - 4; currpos++) // Searching for "fmt "
            {
                file.Position = currpos;
                if (reader.ReadByte() != 0x66) continue;
                if (reader.ReadByte() != 0x6D) continue;
                if (reader.ReadByte() != 0x74) continue;
                if (reader.ReadByte() != 0x20) continue;
                break;
            }
            if (currpos >= file.Length - 12) throw new WaveFormatException("Plik nie jest formatu RIFF WAVE IEEE Float.");

            reader.ReadInt32(); // FMT size

            if (reader.ReadInt16() != 3) throw new WaveFormatException("Plik RIFF WAVE nie jest w formacie IEEE Float.");

            channels = Convert.ToByte(Math.Min(reader.ReadInt16(), byte.MaxValue));

            samplerate = (uint)Math.Abs(reader.ReadInt32());

            byterate = (uint)Math.Max(reader.ReadInt32(), 0);

            blockalign = (ushort)Math.Max((int)reader.ReadInt16(), 0);

            bitdepth = (byte)Math.Abs(Math.Min(reader.ReadInt16(), byte.MaxValue));

            // Search for data chunk
            for(currpos = file.Position; currpos < file.Length-4; currpos++)
            {
                file.Position = currpos;
                if (reader.ReadByte() != 0x64) continue;
                if (reader.ReadByte() != 0x61) continue;
                if (reader.ReadByte() != 0x74) continue;
                if (reader.ReadByte() != 0x61) continue;
                break;
            }
            if (currpos >= file.Length - 4) throw new WaveFormatException("Plik nie posiada danych");

            int size = reader.ReadInt32();

            samplescount = (ulong)Math.Max(blockalign > 0 ? (size / blockalign) : 0, 0);

            dataStartpos = (ulong)Math.Max(file.Position, 0);
        }
        #endregion

        #region Methods
        public float GetSample(byte channelIndex, ulong index)
        {
            if (channelIndex >= channels) throw new IndexOutOfRangeException("Channel index is to large.");
            lock (this._locker)
            {
                if (bitdepth == 32)
                {
                    try { file.Position = Convert.ToInt64(dataStartpos + index * 4 * channels) + Convert.ToInt64(channelIndex * 4); }
                    catch (ArgumentOutOfRangeException) { throw new IndexOutOfRangeException("Index value is to large"); }
                    return reader.ReadSingle();
                }
                else
                    throw new WaveFormatException("Required bit depth is 32.");
            }
        }

        public float[,] GetSamplesAsArray()
        {
            float[,] ret = new float[samplescount, channels];
            for (ulong i = 0; i < samplescount; i++)
            {
                if (bitdepth == 32)
                {
                    try
                    {
                        ret[i, 0] = reader.ReadSingle();
                        if (channels == 2)
                            ret[i, 1] = reader.ReadSingle();
                    }
                    catch(EndOfStreamException) { break; }
                }
                else
                    throw new WaveFormatException("Required bit depth is 32.");
            }
            return ret;
        }

        public async Task<float[,]> GetSamplesAsArrayAsync()
        {
            float[,] ret = new float[samplescount, channels];
            for (ulong i = 0; i < samplescount; i++)
            {
                if (bitdepth == 32)
                {
                    try
                    {
                        ret[i, 0] = reader.ReadSingle();
                        if (channels == 2)
                            ret[i, 1] = reader.ReadSingle();
                    }
                    catch (EndOfStreamException) { break; }
                }
                else
                    throw new WaveFormatException("Required bit depth is 32.");
            }
            return ret;
        }

        public async Task<float[,]> GetSamplesAsArrayAsync(CancellationToken cancel, IProgress<float> progress)
        {
            float[,] ret = new float[samplescount, channels];
            for (ulong i = 0; i < samplescount; i++)
            {
                if (cancel.IsCancellationRequested) return ret;
                if (bitdepth == 32)
                {
                    try
                    {
                        ret[i, 0] = reader.ReadSingle();
                        if (channels == 2)
                            ret[i, 1] = reader.ReadSingle();
                    }
                    catch (EndOfStreamException) { break; }
                }
                else
                    throw new WaveFormatException("Required bit depth is 32.");
                progress.Report((i+1)/samplescount);
            }
            return ret;
        }
        #endregion
    }

    public class WaveFormatException : ApplicationException
    {

        public WaveFormatException() : base("WaveFormatException") { }
        public WaveFormatException(string message) : base(message) { }
        public WaveFormatException(string message, Exception innerException) : base(message, innerException) { }
    }
}
