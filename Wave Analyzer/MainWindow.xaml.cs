﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SharpGL;
using SharpGL.Enumerations;

using WaveFormat;
using System.Windows.Markup;
using System.ComponentModel;
using System.Globalization;

namespace Wave_Analyzer
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable
    {
        private float[,] samples = new [,] { { -1f },{ 0 },{ 1f } };
        private FileStream file = null;
        private float zoom = 0; // scale
        private int scrollpos = 0;
        private uint trim = 0; // zoom trimming
        private uint autoskip = 0; // autoskiping
        private int move = 0; // mouse moving
        // Mouse events
        private Point clickpos;
        private bool mousedown = false;
        private int startmove = 0;
        private uint samplerate = 0;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            file = new FileStream(@"C:\Users\Mateusz\Desktop\Qvo Vadis.wav", FileMode.Open, FileAccess.Read, FileShare.Read);

            WaveFloatFormat wav = new WaveFloatFormat(file);
            this.samplerate = wav.SampleRate;
            Thread t = new Thread(() =>
            {
                var output = wav.GetSamplesAsArray();
                var maxi = output.GetUpperBound(0);
                var maxj = output.GetUpperBound(1);
                samples = output;
                /*samples = new float[maxi+1,maxj+1];
                Parallel.For(0, maxi,new Action<int>(i=>
                {
                    for (uint j = 0; j <= maxj; j++)
                        samples[i, j] = (float)output[i, j] / int.MaxValue;
                }));*/    
            });
            t.Start();
            t.Join();
        }

        private void WaveformDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL GL = args.OpenGL;
            if (GL == null) return;
            GL.ClearColor(0,0,0,1f); // Background
            GL.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            GL.LoadIdentity();
            GL.MatrixMode(MatrixMode.Projection);
            GL.Scale(0.99f, 0.975f, 0);
            GL.Translate(-1f,0,0f);
            
            if (samples != null)
            {
                uint count = (uint)samples.GetUpperBound(0) + 1;
                uint channels = (uint)samples.GetUpperBound(1) + 1;
                double y = 0;

                if (count < 1)
                {
                    GL.LineWidth(0.1f);
                    GL.PolygonMode(FaceMode.FrontAndBack, PolygonMode.Lines);
                    GL.Begin(BeginMode.Lines);

                    GL.Color(0, 1f, 0);
                    GL.Vertex(0f, 0f);
                    GL.Vertex(2f, 0);

                    GL.End();
                }
                else if (channels == 1)
                {
                    GL.PolygonMode(FaceMode.FrontAndBack, PolygonMode.Lines);
                    GL.Begin(BeginMode.Lines);

                    GL.Color(0.1f,0.1f,0.1f);
                    GL.Vertex(0f,0f);
                    GL.Vertex(2f,0f);

                    GL.End();
                    this.trim = (uint)Math.Round((double)count * zoom, 0);
                    
                    GL.PolygonMode(FaceMode.Front, count-(trim*2) > 50000 ? PolygonMode.Lines : PolygonMode.Points);
                    double x = 0;
                    GL.Begin(count - (trim * 2) > 50000 ? BeginMode.Lines : BeginMode.Points);
                    GL.Color(0, 1f, 0);

                    GL.PointSize(1f);
                    this.autoskip = (uint)Math.Round(((double)count / this.samplerate) / (zoom + 0.5), 0);
                    if (this.move > this.trim) this.move = (int)this.trim;
                    if (this.move < -this.trim) this.move = (int)-this.trim;

                    double dx = 2.0 / (count + 1 - (trim*2)) * (autoskip + 1);
                    for (uint i = (uint)Math.Max(0,this.trim+this.move); i < count-this.trim+this.move; i++)
                    {
                        y = Math.Min(Math.Max(-1f, samples[i,0]), 1f);
                        x += dx;
                        GL.Vertex(x, y);
                    }
                    GL.End();
                }
                else if(channels > 1)
                {
                    // Separation line
                    GL.PolygonMode(FaceMode.Front, PolygonMode.Lines);
                    GL.Color(0.5f, 0, 0);
                    GL.Begin(BeginMode.Lines);

                    GL.Vertex(0f, 0);
                    GL.Vertex(2f, 0);

                    GL.End();

                    GL.PolygonMode(FaceMode.Front, count - (trim * 2) > 50000 ? PolygonMode.Lines : PolygonMode.Points);
                    GL.PointSize(1f);

                    double x = 0;

                    GL.Translate(0, 0.5f, 0);              
                    GL.Begin(count - (trim * 2) > 50000 ? BeginMode.Lines : BeginMode.Points);
                    GL.Color(0, 1f, 0);
                    this.trim = (uint)Math.Round((double)count * zoom, 0);
                    this.autoskip = count - (trim * 2) > 50000 ? (uint)Math.Round(((double)(count - (trim * 2)) / this.samplerate) / (zoom + 1), 0) : 0;
                    double dx = 2.0 / (count + 1 - (trim * 2)) * (autoskip + 1);
                    if (this.move > this.trim) this.move = (int)this.trim;
                    if (this.move < -this.trim) this.move = (int)-this.trim;
                    for (uint i = (uint)Math.Max(0,this.trim+this.move); i < count-this.trim+this.move; i+= autoskip+1)
                    {
                        y = Math.Max(-1f, Math.Min(samples[i,0], 1f))*0.45;
                        x += dx;
                        GL.Vertex(x, y);
                    }
                    GL.End();

                    x = 0;
                    GL.Translate(0, -1f, 0);
                    GL.Begin(count - (trim * 2) > 50000 ? BeginMode.Lines : BeginMode.Points);
                    GL.Color(0, 1f, 0);
                    for(uint i = (uint)Math.Max(0,this.trim+this.move); i < count-this.trim+this.move; i += autoskip+1)
                    {
                        y = Math.Max(-1f, Math.Min(samples[i,1], 1f))*0.45;
                        x += dx;
                        GL.Vertex(x, y);
                    }
                    GL.End();
                }
            }

            GL.Flush();
        }

        private void SpectrumDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL GL = args.OpenGL;
            if (GL == null) return;
            GL.Enable(OpenGL.GL_ALPHA_TEST);
            GL.Enable(OpenGL.GL_BLEND);
            GL.BlendFunc(BlendingSourceFactor.SourceAlpha, BlendingDestinationFactor.OneMinusSourceAlpha);
            //GL.AlphaFunc(AlphaTestFunction.Less, 1f);
            GL.Clear(OpenGL.GL_DEPTH_BUFFER_BIT);
            GL.ClearColor(0,0,0,1f);
            GL.PolygonMode(FaceMode.FrontAndBack, PolygonMode.Lines);
            GL.MatrixMode(MatrixMode.Projection);
            GL.Translate(-1f, -1f, 0);
            GL.Scale(2f, 2f, 0);

            GL.Begin(BeginMode.Lines);

            GL.Color(1f, 1f, 1f, 0.5f);

            GL.Vertex(0,1f); GL.Vertex(0,0);
            GL.Vertex(0.07, 1f); GL.Vertex(0.07, 0);
            GL.Vertex(0.111, 1f); GL.Vertex(0.111, 0);
            GL.Vertex(0.14, 1f); GL.Vertex(0.14, 0);
            GL.Vertex(0.163, 1f); GL.Vertex(0.163, 0);
            GL.Vertex(0.181, 1f); GL.Vertex(0.181, 0);
            GL.Vertex(0.196, 1f); GL.Vertex(0.196, 0);
            GL.Vertex(0.21, 1f); GL.Vertex(0.21, 0);
            GL.Vertex(0.222, 1f); GL.Vertex(0.222, 0);
                                         
            GL.Vertex(0.233, 1f); GL.Vertex(0.233, 0);
            GL.Vertex(0.302, 1f); GL.Vertex(0.302, 0);
            GL.Vertex(0.343, 1f); GL.Vertex(0.343, 0);
            GL.Vertex(0.372, 1f); GL.Vertex(0.372, 0);
            GL.Vertex(0.395, 1f); GL.Vertex(0.395, 0);
            GL.Vertex(0.413, 1f); GL.Vertex(0.413, 0);
            GL.Vertex(0.429, 1f); GL.Vertex(0.429, 0);
            GL.Vertex(0.442, 1f); GL.Vertex(0.442, 0);
            GL.Vertex(0.454, 1f); GL.Vertex(0.454, 0);
                                         
            GL.Vertex(0.465, 1f); GL.Vertex(0.465, 0);
            GL.Vertex(0.535, 1f); GL.Vertex(0.535, 0);
            GL.Vertex(0.576, 1f); GL.Vertex(0.576, 0);
            GL.Vertex(0.605, 1f); GL.Vertex(0.605, 0);
            GL.Vertex(0.628, 1f); GL.Vertex(0.628, 0);
            GL.Vertex(0.646, 1f); GL.Vertex(0.646, 0);
            GL.Vertex(0.661, 1f); GL.Vertex(0.661, 0);
            GL.Vertex(0.675, 1f); GL.Vertex(0.675, 0);
            GL.Vertex(0.687, 1f); GL.Vertex(0.687, 0);

            GL.Vertex(0.698, 1f); GL.Vertex(0.698, 0);
            GL.Vertex(0.767, 1f); GL.Vertex(0.767, 0);
            GL.Vertex(0.808, 1f); GL.Vertex(0.808, 0);
            GL.Vertex(0.837, 1f); GL.Vertex(0.837, 0);
            GL.Vertex(0.86, 1f); GL.Vertex(0.86, 0);
            GL.Vertex(0.878, 1f); GL.Vertex(0.878, 0);
            GL.Vertex(0.894, 1f); GL.Vertex(0.894, 0);
            GL.Vertex(0.907, 1f); GL.Vertex(0.907, 0);
            GL.Vertex(0.919, 1f); GL.Vertex(0.919, 0);

            GL.Vertex(0.93, 1f); GL.Vertex(0.93, 0);
            GL.Vertex(1, 1f); GL.Vertex(1f, 0);

            GL.End();

            GL.Flush();
        }

        private void WaveformZoom(object sender, MouseWheelEventArgs e)
        {
            if ((Keyboard.GetKeyStates(Key.LeftCtrl) & KeyStates.Down) > 0 | (Keyboard.GetKeyStates(Key.RightCtrl) & KeyStates.Down) > 0)
                this.scrollpos = Math.Max(this.scrollpos + e.Delta, 0);
            else
                this.scrollpos = Math.Max(this.scrollpos + e.Delta / 20, 0);
            this.zoom = (float)Math.Max(0, Math.Min((-1.0 / (this.scrollpos + 2)) + 0.5, 0.499999999999999));
        }

        private void Waveform_MouseDown(object sender, MouseButtonEventArgs e)
        {
            clickpos = e.GetPosition(this.Waveform);
            mousedown = true;
            if (this.move > this.trim) this.startmove = (int)this.trim;
            else if (this.move < -this.trim) this.startmove = (int)-this.trim;
            else startmove = move;
        }

        private void Waveform_MouseMove(object sender, MouseEventArgs e)
        {
            if (!mousedown) return;
            double x = (samples.GetUpperBound(0)-(trim*2))*((clickpos.X - e.GetPosition(this.Waveform).X)/1000);
            this.move = (int)Math.Max(-this.trim, Math.Min(this.startmove+x, this.trim));
        }

        private void Waveform_MouseUp(object sender, MouseButtonEventArgs e) { this.mousedown = false; }

        public void Dispose()
        {
            
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {

            this.Dispose();
        }
    }

    public class dBFullScale : MarkupExtension
    {
        public float CurrentValue { get; set; }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            CultureInfo culture = Thread.CurrentThread.CurrentUICulture;
            float dbfs = (float)Math.Round(20 * Math.Log10(Math.Abs(CurrentValue)), 2);
            return dbfs.ToString("N", culture) + " dB";
        }
    }
}
