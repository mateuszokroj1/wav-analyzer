﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using SharpGL;
using SharpGL.Enumerations;

namespace WaveAnalyzer.Controls
{
    public partial class WaveAmplitude : Control
    {
        private IEnumerable<float> channel1 = new float[0];
        public IEnumerable<float> Channel1
        {
            get => this.channel1;
            set
            {
                this.channel1 = value;
                this.opengl.DoRender();
            }
        }

        private IEnumerable<float> channel2 = new float[0];
        public IEnumerable<float> Channel2
        {
            get => this.channel2;
            set
            {
                this.channel2 = value;
                this.opengl.DoRender();
            }
        }

        public WaveAmplitude()
        {
            InitializeComponent();
            this.opengl.DoRender();
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            this.opengl.Refresh();
        }


        /// <summary>
        /// Rendering frame
        /// </summary>
        private void opengl_OpenGLDraw(object sender, SharpGL.RenderEventArgs args)
        {
            OpenGL GL = this.opengl.OpenGL;
            if (GL == null) return;
            GL.ClearColor(BackColor.R, BackColor.G, BackColor.B, BackColor.A); // Background
            GL.Clear(OpenGL.GL_COLOR_BUFFER_BIT);

            GL.LoadIdentity();
            if(Enabled & Channel1 != null)
            {
                uint c1 = (uint)Channel1.Count();
                uint c2 = (uint)(Channel2?.Count() ?? 0);
                GL.Translate(-1f,0,0);
                if(c1 < 1)
                {
                    GL.PolygonMode(FaceMode.Front,PolygonMode.Lines);
                    GL.Color(0,1f,0);
                    GL.Begin(BeginMode.Lines);

                    GL.Vertex(0,0);
                    GL.Vertex(1,0);

                    GL.End();
                }
                else if(c1 != c2)
                {
                    GL.PolygonMode(FaceMode.Front,PolygonMode.Points);
                    GL.Color(0,1f,0);
                    float dx = 1f / (c1 + 1);
                    float x = 0;
                    GL.Begin(BeginMode.Points);
                    GL.PointSize(1f);
                    foreach(float point in Channel1)
                    {
                        float y = Math.Min(Math.Max(-1f, point), 1f) * 0.9f;
                        x += dx;
                        GL.Vertex(x,y);
                    }
                    GL.End();
                }
                else
                {
                    // Separation line
                    GL.PolygonMode(FaceMode.Front, PolygonMode.Lines);
                    GL.Color(0.7f, 0, 0);
                    GL.Begin(BeginMode.Lines);

                    GL.Vertex(0, 0);
                    GL.Vertex(1, 0);

                    GL.End();

                    GL.PolygonMode(FaceMode.Front, PolygonMode.Points);
                    GL.Color(0,1f,0);
                    GL.PointSize(0.1f);

                    float x = 0;
                    float dx = 1f/(c2+1);
                    GL.Translate(0,0.5f,0);
                    GL.Scale(0,0.5f,0);
                    GL.Begin(BeginMode.Points);
                    foreach(float point in Channel1)
                    {
                        float y = Math.Max(-1f,Math.Min(point,1f)) * 0.8f;
                        x += dx;
                        GL.Vertex(x,y);
                    }
                    GL.End();

                    GL.Translate(0,-1f,0);
                    GL.Begin(BeginMode.Points);
                    foreach(float point in Channel2)
                    {
                        float y = Math.Max(-1f, Math.Min(point, 1f)) * 0.8f;
                        x += dx;
                        GL.Vertex(x, y);
                    }
                    GL.End();
                }
            }

            GL.Flush();
        }
    }
}
