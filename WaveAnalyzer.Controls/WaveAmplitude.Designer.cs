﻿namespace WaveAnalyzer.Controls
{
    partial class WaveAmplitude
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.opengl = new SharpGL.OpenGLControl();
            this.scroll = new System.Windows.Forms.HScrollBar();
            ((System.ComponentModel.ISupportInitialize)(this.opengl)).BeginInit();
            this.SuspendLayout();
            // 
            // opengl
            // 
            this.opengl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.opengl.DrawFPS = false;
            this.opengl.Location = new System.Drawing.Point(0, 0);
            this.opengl.Name = "opengl";
            this.opengl.OpenGLVersion = SharpGL.Version.OpenGLVersion.OpenGL2_0;
            this.opengl.RenderContextType = SharpGL.RenderContextType.DIBSection;
            this.opengl.RenderTrigger = SharpGL.RenderTrigger.TimerBased;
            this.opengl.Size = new System.Drawing.Size(150, 150);
            this.opengl.TabIndex = 0;
            this.opengl.OpenGLDraw += new SharpGL.RenderEventHandler(this.opengl_OpenGLDraw);
            // 
            // scroll
            // 
            this.scroll.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.scroll.Location = new System.Drawing.Point(0, 0);
            this.scroll.Name = "scroll";
            this.scroll.Size = new System.Drawing.Size(80, 17);
            this.scroll.TabIndex = 0;
            // 
            // WaveAmplitude
            // 
            this.BackColor = System.Drawing.Color.Black;
            ((System.ComponentModel.ISupportInitialize)(this.opengl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private SharpGL.OpenGLControl opengl;
        private System.Windows.Forms.HScrollBar scroll;
    }
}
